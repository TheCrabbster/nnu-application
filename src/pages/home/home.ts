import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  stuffs = [
    {
        "calendarId": "17bc820e-e4f4-4208-b8df-a1253c0e4424",
        "calendarName": "Music",
        "id": "f61a2b91-8408-4e0b-a399-1e37dbdd30f4",
        "title": "CONCERT BAND SERVICE PROJECT",
        "description": "The concert band will be completeing their service project here in the united states of american on this coming date. It should be great.",
        "start": "2019-09-27T13:30:00",
        "end": "2019-09-29T22:00:00",
        "userChangedStartOrEndDate": false,
        "allDay": false,
        "location": "Shiloh Bible Camp, Donnelley, Idaho",
        "isReadOnly": true,
        "textColor": null,
        "backgroundColor": null,
        "borderColor": null,
        "eventRepeats": false,
        "parentRecurringEventId": null,
        "recurrenceEndDate": null,
        "recurrenceInterval": null,
        "recurrenceFrequency": null,
        "recurrenceOffset": null,
        "recurrenceFrequencyWeekdays": null,
        "recurrenceMonths": null,
        "eventSource": "jics",
        "eventType": "default",
        "viewRoute": "event.view",
        "tags": [{"id": "90d4f7e3-ab93-4a19-ac7e-f8d836517abd","isActive": true,"name": "Display on Public Website"}],
        "tags2": ["Music", "Service"]
      },
      {
        "calendarId": "290bddc6-4ffb-4a46-a292-4d4bb1ee75f9",
        "calendarName": "Athletics",
        "id": "f8763bbf-cbd0-4da4-a430-cc2ce8d3c933",
        "title": "Women's basketball at Montana Western",
        "description": "Exhibition. What kind of description is that. Really lame one that's for sure. At least you could say: an exhibition match. You act as if this description tag is for computers not highely verbal processors. I wonder who inputs these.",
        "start": "2019-11-02T18:00:00",
        "end": "2019-11-02T20:00:00",
        "userChangedStartOrEndDate": false,
        "allDay": false,
        "location": "Dillon, Mont.",
        "isReadOnly": true,
        "textColor": null,
        "backgroundColor": null,
        "borderColor": null,
        "eventRepeats": false,
        "parentRecurringEventId": null,
        "recurrenceEndDate": null,
        "recurrenceInterval": null,
        "recurrenceFrequency": null,
        "recurrenceOffset": null,
        "recurrenceFrequencyWeekdays": null,
        "recurrenceMonths": null,
        "eventSource": "jics",
        "eventType": "default",
        "viewRoute": "event.view",
        "tags": [],
        "tags2": ["Athletics", "Womens-Basketball"]
      }
  ];
  events: Observable<any>;
  url: String = "https://www.nnu.edu/hayden.php?start=2019-12-10&end=2022-11-10";


  constructor(public navCtrl: NavController, public httpClient: HttpClient) {
   // this.events = this.httpClient.get(this.url);
   /* this.events.subscribe(data => {
      if (data != "")
      {
        this.stuffs = data;
      }
    }) */
  }

  doRefresh(refresher)
  {
  	console.log("smurp");
  	setTimeout(() => { 
      refresher.complete();
    }, 1000);
  }
}
