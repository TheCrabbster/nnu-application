import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomecomingPage } from './homecoming';

@NgModule({
  declarations: [
    HomecomingPage,
  ],
  imports: [
    IonicPageModule.forChild(HomecomingPage),
  ],
})
export class HomecomingPageModule {}
